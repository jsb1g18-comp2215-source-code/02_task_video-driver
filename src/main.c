/* COMP2215: Task 02---EXTRA_WORK */
/* For La Fortuna board.            */


#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd.h"
#include <util/delay.h>
#include <stdio.h>


#define BUFFSIZE 256

void init(void);

void main(void) {
    init();
    for (;;) {
      display_string("Welcome to scrolling demo!\n");
      _delay_ms(1000);
      clear_screen();
      for(uint16_t i = 0;i < 100; i ++) {
        char s[18];
        sprintf(s,"%d. - Hello World!\n",i);
        display_string(s);
        _delay_ms(50);
      }
      clear_screen();
    }
}


void init(void) {
    /* 8MHz clock, no prescaling (DS, p. 48) */
    CLKPR = (1 << CLKPCE);
    CLKPR = 0;

    init_lcd();

    set_frame_rate_hz(61); /* > 60 Hz  (KPZ 30.01.2015) */
    /* Enable tearing interrupt to get flicker free display */
    EIMSK |= _BV(INT6);
}
